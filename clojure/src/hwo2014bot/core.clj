(ns hwo2014bot.core
  (:require [clojure.data.json :as json])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn map-function-on-map-vals [m f]
  (reduce (fn [altered-map [k v]] (assoc altered-map k (f v))) {} m))

(defn send-message
  ([channel msg]
   (println msg)
   (enqueue channel (json/write-str msg)))
  ([channel type data]
   (send-message channel {:msgType type :data data})))

(defn read-message
  ([channel]
   (json->clj
     (try
       (let [msg (wait-for-message channel)]
         (println msg)
         msg)
       (catch Exception e
         (println (str "ERROR: " (.getMessage e)))))))
  ([channel type]
   (let [msg (read-message channel)]
     (if (= type (:msgType msg))
       (:data msg)
       (read-message channel type)))))

(defn connect-client-channel [host port]
  (wait-for-result
    (tcp-client {:host  host,
                 :port  port,
                 :frame (string :utf-8 :delimiters ["\n"])})))

(defn next-lane [lanes position]
  (get lanes (:lane-index position)))

(defn abs [n] (max n (- n)))

(def car-length 50)       ; Might change
(def pi 3.1415926535898)  ; Should stay the same

(defn sign [val] (if (= 0.0 val) 1 (/ val (abs val))))

(defn piece-length [piece lane]
  (if (nil? (:length piece))
    (let [radius (:radius piece)
          distanceFromCenter (:distanceFromCenter lane)
          angle (:angle piece)]
      (abs (* (- radius (* (sign angle) distanceFromCenter))
              (* pi
                 (/ angle 180.0)))))
    (:length piece)))

(defn neighbor-lanes [lanes lane]
  ;(println lanes lane)
  (let [index (:index lane)]
    (remove nil? [lane (get lanes (- index 1)) (get lanes (+ index 1))])))

(defn shortest-dist [pieces lanes lane]
  (if (empty? pieces)
    0
    (+ (piece-length (first pieces) lane)
       (if (:switch (first (rest pieces)))
         (apply min (map (fn [lane] (shortest-dist (rest pieces) lanes lane))
                         (neighbor-lanes lanes lane)))
         (shortest-dist (rest pieces) lanes lane)))))

; Such a nice function.
; Such a shame.
; I just don't have time to optimize speed and logic for lanes.
(defn best-lane [pieces lanes lane]
  (reduce (fn [lane0 lane1]
            (if (<= (shortest-dist pieces lanes lane0)
                    (shortest-dist pieces lanes lane1))
              lane0
              lane1))
          (neighbor-lanes lanes lane)))

(defn switch-lane? [pieces lanes lane]
  (let [better-index (:index (best-lane pieces lanes lane))
        current-index (:index lane)]
    (if (not= better-index current-index)
      better-index
      nil)))

(defn switch-lane [pieces lanes lane]
  (let [better-index (:index (best-lane pieces lanes lane))
        current-index (:index lane)]
    (if (not= better-index current-index)
      (if (< better-index current-index) "Left" "Right")
      false)))

(defn car-position [color cars]
  (if (= color (:color (:id (first cars))))
    (first cars)
    (car-position (rest cars))))

(defn remaining-pieces [pieces index0 index1]
  (if (not= index0 index1)
    (next pieces)
    pieces))

(defn piece-speed [position0 position1]
  (and position0 position1
       (if (= (:piece-index position1)
              (:piece-index position0))
         (- (:dist position1)
            (:dist position0))
         (+ (:dist position1)
            (- (piece-length (first (:pieces position0)) (:lane position0))
               (:dist position0))))))

(defn solve-speed [speed0 throttle consts]
  (and speed0 throttle (:acceleration consts) (:drag consts)
       (- (+ speed0 (* throttle (:acceleration consts))) (* (:drag consts) speed0))))

(defn solve-drag [speed0 speed1 throttle consts]
  (assert throttle 1.0)
  (- (/ (- (- speed1 speed0) (* throttle (:acceleration consts))) speed0)))

(defn solve-acceleration [speed0 speed1 throttle]
  (* (- speed1 speed0) throttle))

(def crash-angle 55)
(def angle-const 240)

(defn target-angle [speed0 radius direction consts]
  (and (:corner-factor consts)
       (if (= radius 0.0)
         0.0
         (* direction (max 0 (- (* (/ speed0 (Math/sqrt radius)) (:corner-factor consts))
                                angle-const))))))

(defn lane-radius [piece lane]
  (or (and (:radius piece)
           (- (:radius piece) (* (sign (:angle piece)) (:distanceFromCenter lane))))
      0.0))

(def inertia-cumul-factor 0.9)
(def inertia-spring-factor 0.1)
(def inertia-speed-factor 0.0125)
(def accel-corr-factor 12.0)

(defn next-inertia [speed0 angle1 radius inertia0 direction consts]
  ;(println "Next inertia from" speed0 angle1 radius inertia0 consts)
  (and speed0 angle1 radius direction (:corner-factor consts)
       (let [target-angle (target-angle speed0 radius direction consts)
             inertia0 (or inertia0 0.0)]
         (-> (* inertia-cumul-factor inertia0)
             (+ (* (- target-angle angle1)
                   inertia-spring-factor))))))

(defn accel-corr-1 [speed0 speed1 angle0]
  (-> (- speed1 speed0)
      (* (Math/cos (* (/ (- 90 angle0) 180) pi)))
      (* accel-corr-factor)))

(defn accel-corr [speed0 speed1 angle0] 0.0)
(defn break-corr [speed0 speed1 direction]
  ;(println speed0 speed1 direction)
  (* direction (Math/sqrt (max 0.0 (- speed0 speed1)))))
(defn next-angle [speed0 speed1 angle direction inertia]
  ;(println "Next angle from" speed0 speed1 angle inertia)
  ;(print (str speed0 "\t" angle "\t" inertia "\n"))
  (-> angle
      (+ (* inertia inertia-speed-factor speed0))
      (+ (break-corr speed0 speed1 direction))))

(defn get-angle [piece]
  (if (:angle piece) (:angle piece) 0.0))

(defn solve-throttle [current-speed target-speed consts]
  ;(println current-speed target-speed consts)
  (max 0.0 (min 1.0 (/ (- (+ target-speed (* (:drag consts) current-speed)) current-speed) (:acceleration consts)))))

(defn can-hit-speed? [current-speed target-speed current-pos target-pos consts]
  (if (< current-speed target-speed)
    true
    (if (>= current-pos target-pos)
      false
      (can-hit-speed?
        (solve-speed current-speed (solve-throttle current-speed target-speed consts) consts)
        target-speed
        (+ current-pos current-speed)
        target-pos
        consts))))

(defn max-speed-for-radius [radius consts]
  (/ (* (Math/sqrt radius) (+ crash-angle 240)) (:corner-factor consts)))

(defn can-hit-radius? [radius current-speed current-pos target-pos consts]
  (can-hit-speed? current-speed (max-speed-for-radius radius consts) current-pos target-pos consts))

(defn dist-to-next-angle-piece [pieces lane]
  (if (:radius (first pieces))
    0
    (+ (piece-length (first pieces) lane)
       (dist-to-next-angle-piece (rest pieces) lane))))

(defn next-angle-piece [pieces]
  (if (:radius (first pieces))
    (first pieces)
    (next-angle-piece (rest pieces))))

(defn can-hit-next-radius-piece? [speed position pieces lane consts]
  (let [radius (lane-radius (next-angle-piece (rest pieces)) lane)]
    (can-hit-radius? radius
                     speed
                     0
                     (+ (dist-to-next-angle-piece (rest pieces) lane) (- (piece-length (first pieces) lane) position))
                     consts)))

(defn solve-first-inertia [angle speed]
  (and angle speed (> (abs angle) 0.0)
       (/ angle (* inertia-speed-factor speed))))

(defn solve-corner-factor [angle radius speed]
  (or (and (not= angle 0.0) (not= radius 0.0)
           (-> (/ (solve-first-inertia angle speed) inertia-spring-factor)
               (+ 240)
               (* (Math/sqrt radius))
               (/ speed)))
      nil))

(defn solve-constants [consts history throttle radius]
  (let [speed0 (-> history next next first :speed)
        speed1 (-> history next first :speed)
        speed2 (-> history first :speed)
        angle (-> history first :angle)]
    (or (and speed2 throttle (nil? (:acceleration consts))
             (assoc consts :acceleration (solve-acceleration 0.0 speed2 throttle)))
        (and speed1 speed2 throttle (:acceleration consts) (nil? (:drag consts))
             (assoc consts :drag (solve-drag speed1 speed2 throttle consts)))
        (and angle radius speed0 (nil? (:corner-factor consts))
             (assoc consts :corner-factor (solve-corner-factor angle radius speed0)))
        consts)))

; A car is actually a list that describes its history. First item in the list is the car's current  state.
; I need to come up with better names for these things... or maybe this isn't idiomatic clojure.
(defrecord Position [speed angle inertia lane pieces piece-index lande-index dist lap])
(defn json->Position
  [pos]
  {:dist        (-> pos :piecePosition :inPieceDistance)
   :piece-index (-> pos :piecePosition :pieceIndex)
   :lane-index  (-> pos :piecePosition :lane :endLaneIndex)
   :lap         (-> pos :lap)
   :angle       (-> pos :angle)})

(defrecord Track [pieces lanes])

(defn map-on-map [map func]
  (reduce (fn [m [k v]] (assoc m k (func v))) {} map))

(defn update-turbo [turbo]
  (or (and (:activated turbo) (update-in turbo [:turboDurationTicks] dec))
      turbo))

; This is somehow magically ugly.
(defn update-car [history new-position track consts]
  (let [lane (get (:lanes track) (:lane-index new-position))
        turbo (update-turbo (-> history first :turbo))
        pieces (or (remaining-pieces
                     (-> history first :pieces)
                     (-> history first :piece-index)
                     (-> new-position :piece-index))
                   (drop (:piece-index new-position) (cycle (:pieces track)))) ; <------------- I'm in love.
        speed (piece-speed (first history) new-position)
        inertia (or (next-inertia (-> history next next first :speed)
                                  (-> history first :angle)
                                  (-> history first :pieces first (lane-radius lane))
                                  (-> history first :inertia)
                                  (-> history first :pieces first get-angle sign)
                                  consts)
                    (solve-first-inertia (:angle new-position)
                                         (-> history next first :speed)))]
    (conj history (merge new-position {:speed   speed
                                       :inertia inertia
                                       :lane    lane
                                       :turbo   turbo
                                       :pieces  pieces
                                       :lap (-> history first :lap)}))))

(defn alter-current [history keys value]
  (conj (next history)
        (assoc-in (first history) keys value)))

; More like "handle messages" but the result is updated cars so...
(defn update-cars [cars msg channel track consts]
  ; I'm such a noob for not getting case to work :(
  (or (and (= "carPositions" (:msgType msg))
           (reduce (fn [cars car]
                     (let [key (-> car :id :color keyword)
                           history (or (cars key) '())]
                       (assoc cars key (update-car
                                         history
                                         (json->Position car)
                                         track
                                         consts))))
                   cars (:data msg)))
      (and (= "turboAvailable" (:msgType msg))
           (do (println "turbo available")
               (update-cars (map-on-map cars (fn [history] (alter-current history [:turbo] (:data msg))))
                            (read-message channel) channel track consts)))
      (and (= "turboStart" (:msgType msg))
           (do (println "turbo start")
               (update-cars (update-in cars [(-> msg :data :color keyword)] (fn [history] (alter-current history [:turbo :activated] true)))
                            (read-message channel) channel track consts)))
      (and (= "turboEnd" (:msgType msg))
           (do (println "turbo end")
               (update-cars (update-in cars [(-> msg :data :color keyword)] (fn [history] (alter-current history [:turbo] nil)))
                            (read-message channel) channel track consts)))
      (and (= "crash" (:msgType msg))
           (do (println "crash")
               (update-cars (update-in cars [(-> msg :data :color keyword)] (fn [history] (-> history (alter-current [:turbo] nil) (alter-current [:crashed] true))))
                            (read-message channel) channel track consts)))
      (and (= "spawn" (:msgType msg))
           (do (println "spawn")
               (update-cars (update-in cars [(-> msg :data :color keyword)] (fn [history] (-> history (alter-current [:turbo] nil) (alter-current [:crashed] false))))
                            (read-message channel) channel track consts)))
      (and (= "dnf" (:msgType msg))
           (do (println "dnf")
               (update-cars (assoc-in cars [(-> msg :data :color keyword)] nil)
                            (read-message channel) channel track consts)))
      (and (= "lapFinished" (:msgType msg))
           (do (println "lap finished")
               (update-cars (update-in cars [(-> msg :data :car :color keyword)] (fn [history] (alter-current history [:lap] (-> msg :data :raceTime :laps))))
                            (read-message channel) channel track consts)))
      (recur cars (read-message channel) channel track consts)))

(defn get-dist [dist speed piece lane]
  (if (> (+ dist speed) (piece-length piece lane))
    (-> (+ dist speed) (- (piece-length piece lane)))
    (+ dist speed)))

(defn turbo-available? [current]
  (and (-> current :turbo) (not (-> current :turbo :activated))))

;; Check if a crash is inevitable
(defn crash? [speed0 speed1 speed2 angle dist inertia0 pieces lane track consts turbo min-throttle ticks]
  ;(when turbo (println "crash?" (first pieces) min-throttle speed0 speed1 speed2 angle inertia0))
  (and (:corner-factor consts)
       (let [piece (first pieces)
             radius (lane-radius piece lane)
             direction (sign (get-angle piece))
             inertia0 (or inertia0 (solve-first-inertia angle speed0))
             inertia1 (next-inertia speed0 angle radius inertia0 (sign (or (:angle piece) 0.0)) consts)
             angle (next-angle speed0 speed1 angle direction inertia1)
             turbo-factor (or (:turboFactor turbo) 1.0)
             throttle (* min-throttle turbo-factor)
             speed3 (solve-speed speed2 throttle consts)]
         ;(println "Crash? with speed" speed3 "throttle" throttle "turbo" turbo)
         (if (and (> ticks 0) (< (abs angle) crash-angle))
           (crash? speed1                                   ;TODO: recur?
                   speed2
                   speed3
                   angle
                   (get-dist dist speed1 piece lane)
                   inertia1
                   (if (> (+ dist speed1) (piece-length piece lane))
                     (next pieces)
                     pieces)
                   lane
                   track
                   consts
                   (and turbo (update-in turbo [:turboDurationTicks] dec))
                   min-throttle
                   (dec ticks))
           (> (abs angle) crash-angle)))))

(defn accelerate-in-curve [current consts]
  (let [radius (lane-radius (-> current :pieces first) (-> current :lane))]
    (if (or (nil? (:drag consts)) (nil? (:speed current)))
      1.0
      (if (> radius 0.0)
        (solve-throttle (:speed current) (+ (:speed current) 0.01) consts)
        (solve-throttle (:speed current) 6.0 consts)))))

(defn throttle-for-angle-test [current consts]
  (let [radius (lane-radius (-> current :pieces first) (-> current :lane))]
    ;(println (:speed current) (:angle current) radius)
    (if (and (= radius 0.0)
             (not= (:angle current) 0.0))
      (solve-throttle (:speed current) 7.11 consts)
      (if (or (nil? (:drag consts)) (nil? (:speed current)))
        1.0
        (solve-throttle (:speed current) 7.1 consts)))))
;(solve-throttle (:speed current) (+ (:speed current) 0.01) consts)
; Gives an optimal throttle that keeps the car on track.
; The trick simple: check if a crash is inevitable. If not, use throttle.
; TODO: Give an optimal value instead of 0 or 1.
(defn optimize-throttle
  ([history track consts start test-turbo]
   (if (or (nil? (:drag consts)) (nil? (:speed (first history))))
     start
     (if (or (nil? (:corner-factor consts)) (nil? (:angle (first history))))
       (solve-throttle (:speed (first history)) 7.0 consts)
       (let [turbo (-> history first :turbo)
             speed0 (-> history first :speed)
             speed1 (solve-speed speed0 (* 1.0 (or (:turboFactor turbo) 1.0)) consts)
             speed2 (solve-speed speed1 (* 1.0 (or (:turboFactor turbo) 1.0)) consts)]
         ;(println "Test crash with throttle" throttle)
         (if (crash? speed0
                     speed1
                     speed2
                     (-> history first :angle)
                     (-> history first :dist)
                     (-> history first :inertia)
                     (-> history first :pieces)
                     (-> history first :lane)
                     track
                     consts
                     (if (or test-turbo (:activated turbo))
                       turbo
                       nil)
                     (or (and test-turbo 1.0)
                         (and (:activated turbo) 0.0)
                         0.2)
                     (or (and test-turbo 30) 120))
           0.0
           start)))))
  ([history track consts start] (optimize-throttle history track consts start false)))

(defn exit-piece? [current]
  (and (:dist current) (:speed current)
       (> (+ (:dist current) (:speed current))
          (piece-length (-> current :pieces first) (:lane current)))))

; Not tested
(defn dist-to-next-piece [current]
  (- (piece-length (-> current :pieces first) (-> current :lane))
     (-> current :dist)))

; Trying shortcut for anon funcs. Niiiice....
(defn cars-on-lane [cars index]
  (map #(first (get % 1))
       (filter #(= index (-> (second %) first :lane :index)) cars)))

(defn dist-to-car [current car]
  (loop [pieces (:pieces current)
         i (- (:piece-index car) (:piece-index current))
         dist 0.0]
    (if (= 0 i)
      (+ dist (- (:dist car) (:dist current)))
      (recur (next pieces)
             (- i (sign i))
             (+ dist (* (sign i) (piece-length (first pieces) (:lane current))))))))


; Find cars in front of the current position on the same lane
(defn cars-in-front [current others]
  (filter #(> (dist-to-car current %) 0.0)
          (cars-on-lane others (-> current :lane :index))))

; Find the car in front closest to current position on the same lane
(defn car-in-front [current others]
  (first (sort (comp (fn [car0 car1]
                       (< (dist-to-car current car0)
                          (dist-to-car current car1))))
               (cars-in-front current others))))

(def bump-dist (* car-length 4.0))
(def bump-angle 30.0)
; Is this a good moment to do some bumping?
(defn bump? [current car]
  (and (< (dist-to-car current car) bump-dist)
       (> (abs (:angle car)) (abs (:angle current)))
       (< (abs (:angle current)) 50.0)
       (do (println "BUMP!") true)))

(def follow-dist (* car-length 4.0))
; Should we follow the car (not hit it)? This was proven not to be a very good tactic...
(defn follow? [current car]
  (and (< (dist-to-car current car) follow-dist)
       (< (:angle current) (:angle car))))

(defn act [history cars track consts tick]
  ;(println "act")
  (let [current (first history)
        switch (switch-lane (take 40 (-> current :pieces next)) (:lanes track) (:lane current))
        next-car (car-in-front (first history) cars)]
    ;(println "car search done")
    (and (seq next-car) (println "Dist to next car" (dist-to-car current next-car)))
    (or (and false (= tick 60) switch {:msgType "switchLane" :data switch})
        (and (seq next-car) (bump? current next-car)
             (if (turbo-available? current)
               {:msgType "turbo" :data "Bump Turpo!!"}
               {:msgType "throttle" :data 1.0}))
        (and (seq next-car) (follow? current next-car)
             {:msgType "throttle" :data (solve-throttle (:speed current) (:speed next-car) consts)})
        (and (turbo-available? current)
             (>= (dist-to-next-angle-piece (:pieces current) (:lane current)) 100.0)
             (= 1.0 (optimize-throttle history track consts 1.0 true))
             {:msgType "turbo" :data "Turpoo!!"})
        {:msgType "throttle" :data (optimize-throttle history track consts 1.0 false) :gameTick tick})))
;(optimize-throttle history track consts 1.0 false)
;(throttle-for-angle-test current consts)
(defn game-loop [channel color cars track consts tick]
  (let [history ((keyword color) cars)
        current (first history)
        radius (lane-radius (-> current :pieces first) (-> current :lane))
        consts (solve-constants consts
                                history
                                1.0
                                radius)
        action (act ((keyword color) cars) cars track consts tick)]
    (print (str (* radius (sign (or (-> current :pieces first :angle) 1.0))) "\t" (:speed current) "\t" (:angle current) "\n"))
    ;(println action)
    (send-message channel action)
    (let [msg (read-message channel)]
      (if (= (:msgType msg) "gameEnd")
        consts
        (recur
          channel
          color
          ; Update cars with incoming message
          (update-cars cars msg channel track consts)
          track
          consts
          (inc tick))))))

(defn initialize [channel]
  (let [color (-> (read-message channel "yourCar") :color)
        gameInit (read-message channel "gameInit")
        carPositions (read-message channel)]
    (read-message channel "gameStart")
    ;(write-message conn "throttle" 1.0)
    (println "Start game loop")
    ; Initialize the race with constants returned by qualification round.
    (let [consts (game-loop
                   channel
                   color
                   (update-cars {} carPositions channel (-> gameInit :race :track) {})
                   (-> gameInit :race :track)
                   {}
                   0)
          gameInit (read-message channel "gameInit")
          carPositions (read-message channel)
          ]
      (read-message channel "gameStart")
      (game-loop
        channel
        color
        (update-cars {} carPositions channel (-> gameInit :race :track) {})
        (-> gameInit :race :track)
        consts
        0))))

(defn -main [& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel "join" {:name botname :key botkey})
    (initialize channel)))

(def testserver {:name "hakkinen.helloworldopen.com" :port 8091})
(def user {:key "7NGYoOTE+iKjXA" :name "tiko"})
(defn start []
  (let [channel (connect-client-channel (:name testserver) (:port testserver))]
    (send-message channel "createRace" {:botId user :trackName "suzuka" :carCount 1})
    (initialize channel)))

(defn join [count name]
  (let [channel (connect-client-channel (:name testserver) (:port testserver))]
    (send-message channel "joinRace" {:botId {:key (:key user) :name name} :trackName "suzuka" :carCount count})
    (initialize channel)))